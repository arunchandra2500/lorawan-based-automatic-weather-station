# Automatic Weather Station

LoRaWAN based Automatic weather station is a system which measures the environmental parameters and transmits the data at regular intervals.

List of Parameters recorded

- Temperature
- Humidity
- Atmospheric Pressure
- Rain measurement
- Wind speed
- Wind Direction

LoRaWAN transmission is every 15 minutes.

The application code is written on top of the I-cube-lrwan package by STM32. 
## Getting Started


- Make sure that you have a STM32 based Board.

- Install LoRaWAN software expansion code from [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html) . Copy the code to ~/STM32CubeIDE/workspace/

- Select board : B-L072Z-LRWAN1 Development Board

## Prerequisites

STM32CubeIDE 1.1.0[Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/lorawan-based-automatic-weather-station/-/blob/master/LICENSE.md) file for details

## Acknowledgments

- LoRaWAN software expansion code of [Semtech](https://www.st.com/en/embedded-software/i-cube-lrwan.html) and Stackforce
