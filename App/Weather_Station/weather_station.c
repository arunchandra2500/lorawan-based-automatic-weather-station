/*
 * weather_station.c
 *
 *  Created on: Dec 1, 2020
 *  Author: Akshay, Ajmi-- ICFOSS
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"

#include "weather_station.h"
#include "util_console.h"
#include "lora.h"
#include "i2c.h"
#include "sht2x_for_stm32_hal.h"
//#include "si1145.h"


/*Anemometer timer*/

#define WS_MEASURMENT_TIME   5000  /* wind speed measurement time 5 sec - defined in ms */
#define WS_RADIUS   0.000072  /* radius in kilometer */

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

//BMP280_HandleTypedef bme280Sensor1; /*   handler for bme280 sensor  */

uint16_t rainGaugetipCount = 0; /*   rainCount increment for every interrupt rain fall */

uint16_t windSpeedRotationsCount = 0; /*   Anemometer count */

extern TimerEvent_t InterruptTimer; /*   handler for interrupt timer(wind speed calc)  */

extern LoraFlagStatus AppProcessRequest; /*   LORA send process flag   */

//I2C_HandleTypeDef hi2c1; /*   handler for i2c   */

/*  readBatteryLevel   */
/**
 * Created on: Nov 20, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay, Ajmi-- ICFOSS
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
uint16_t readBatteryLevel(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(ADC_CHANNEL_4);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}

/*  getWindDirection  */
/**
 * Created on: Nov 20, 2020
 * Last Edited: Jan 6, 2021
 * Author: Sruthi, Akshay -- ICFOSS
 *
 * @brief  read direction of wind from channel 0 using direction calibrated values
 * @param  none
 * @retval wind direction
 *
 **/
uint16_t getWindDirection() {
	uint16_t analogValue = 0; /*   store the analog value  */
	float direction = 0; /*   actual angle of wind direction functions  */

	float compareDirectionDeg[] = { 112.5, 67.5, 90, 157.5, 135, 202.5, 180,
			22.5, 45, 247.5, 225, 337.5, 0, 292.5, 315, 270 }; /*calibrated values for wind direction readings  */
	uint16_t dirCalibValueMin[] = { 231, 295, 371, 453, 653, 864, 1048, 1482,
			1839, 2266, 2468, 2731, 3010, 3188, 3432, 3734 }; /* calibrated min values  for wind direction*/
	uint16_t dirCalibValueMax[] = { 294, 370, 452, 574, 863, 1001, 1307, 1829,
			2037, 2450, 2729, 2995, 3310, 3413, 3729, 3964 }; /* calibrated max values  for wind direction*/

	/**** {"ESE","ENE","E","SSE","SE","SSW","S","NNE","NE","WSW","SW","NNW","N","WNW","NW","W"} -- directions corresponding to values compareDirectionDeg array  *****/

	/*  power the gpio for reading the analog channel	*/
	enable(WINDDIR_POWER);

	/*	reading analog direction channel	*/
	analogValue = HW_AdcReadChannel(ADC_CHANNEL_0);

	/*  disable power after reading the analog channel	*/
	disable(WINDDIR_POWER);

	for (uint8_t i = 0, k = 0; i <= 15; i++, k++) {
		if (analogValue >= dirCalibValueMin[i]
											&& analogValue <= dirCalibValueMax[i]) {
			direction = compareDirectionDeg[k];
		}

	}

	return (uint16_t) (direction * 10);
}

/*  rainGaugeInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PA9 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable() {
	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(GPIOA, GPIO_PIN_9, &initStruct);
	HW_GPIO_SetIrq(GPIOA, GPIO_PIN_9, 0, rainGaugeTips);
}

/*  rainGaugeTips  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief callback function interrupt on PA9 pin
 * @note this pin PA9 is connected to a Rain gauge Reed sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeTips() {
	rainGaugetipCount++;

}

/*  getAccumulatedRainfall  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval
 *
 **/
uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = ((float) rainGaugetipCount * 0.28) * 100;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}

/*  windSpeedInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note this pin PB14 is connected to a anemometer Reed switch
 * @param none
 * @retval none
 *
 **/
void windSpeedInterruptEnable() {
	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, windSpeedRotations);
}

/*  windSpeedInterruptEnable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief initialise a timer to read anemometer for user defined interval
 * @note WS_MEASURMENT_TIME is the user defined interval
 * @param none
 * @retval none
 *
 **/
void windSpeedTimerEvent() {
	TimerInit(&InterruptTimer, onWindSpeedTimerEvent);
	TimerSetValue(&InterruptTimer, WS_MEASURMENT_TIME);
}

/*  windSpeedRotations  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief callback function interrupt on PB14 pin
 * @note this pin PB14 is connected to a anemometer Reed sensor
 * @param none
 * @retval none
 *
 **/
void windSpeedRotations(void *context) {
	windSpeedRotationsCount++;
}

/*  getWindSpeed  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief calculate wind speed with number of rotations captured in timer event
 *
 * @param none
 * @retval Wind speed in kmph * 10
 *
 **/
uint16_t getWindSpeed() {
	float windSpeedKMH = 0;

	windSpeedRotationsCount = windSpeedRotationsCount / 2;
	windSpeedKMH = (windSpeedRotationsCount * 2.4) / 5; /* 2.4 kmh for 1 rotation per sec 5 sec*/
	windSpeedRotationsCount = 0;

	return (uint16_t) (windSpeedKMH * 10);
}
/*  onWindSpeedTimerEvent  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief disable the interrupt on PB14 after wind speed timer event expires and set the lorawan transmission.
 *
 * @param none
 * @retval none
 *
 **/
void onWindSpeedTimerEvent() {
	/*	disable PB14 interrupt to timer event of WS_MEASURMENT_TIME*/
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_14);
	AppProcessRequest = LORA_SET;
}

/*  SHT20HWInit  */
/**
 * Created on: Jun 25, 2021
 * Last Edited: Jun 26, 2021
 * Author: Arun
 *
 * @brief initialise sht20 with default parameters and i2c1
 * @note none
 * @param none
 * @retval none
 *
 **/
#ifdef ENABLE_SHT20
void sht20init()
{
	enable(BME280_POWER);			/*enable SHT20 module power*/
	SHT2x_Init(&hi2c1);				/* Initializes SHT2x temperature/humidity sensor and sets the resolution. */
	SHT2x_SetResolution(RES_14_12);
}
#endif

/*  readSHT20  */
/**
 * Created on: Jun 25, 2021
 * Last Edited: Jul 30, 2021
 * Author: Arun, Ajmi
 *
 * @brief read data from available API
 * @param sensor data pointer that contains the variables to be read from the sensor
 * @retval none
 *
 **/
#ifdef ENABLE_SHT20
void SHT20Read(awsSensors_t *sensor_data)
{
	enable(BME280_POWER);
	HAL_Delay(50);
	float cel = SHT2x_GetTemperature(1);
	/* Converts temperature to degrees Fahrenheit and Kelvin */
	//float fah = SHT2x_CelsiusToFahrenheit(cel);
	//float kel = SHT2x_CelsiusToKelvin(cel);
	float rh = SHT2x_GetRelativeHumidity(1);
	sensor_data->temperature=cel;
	sensor_data->humidity=rh;
	HAL_Delay(50);
	disable(BME280_POWER);
}
#endif

/*  bme280HWInit  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021
 * Author: Akshay,Sruthi, Ajmi
 *
 * @brief initialise bme280 with default parameters and i2c1
 * @note none
 * @param none
 * @retval none
 *
 **/
#ifdef ENABLE_BME280
void bme280HWInit() {
	bmp280_init_default_params(&bme280Sensor1.params);
	bme280Sensor1.addr = BMP280_I2C_ADDRESS_0;
	bme280Sensor1.i2c = &hi2c1;
}
#endif
/*  bme280HWInit  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021
 * Author: Akshay,Sruthi, Ajmi
 *
 * @brief initialize the sensor using bmp280_init API and check if sensor is initialized
 * @param none
 * @retval sensor init status
 *
 **/
#ifdef ENABLE_BME280
bool bme280IoInit() {
	/*enable bme280 module power*/
	enable(BME280_POWER);
	HAL_Delay(50);

	bool bme280Status = bmp280_init(&bme280Sensor1, &bme280Sensor1.params);
	HAL_Delay(50);
	if (bme280Status)
		return BME280_INIT_SUCCESS;
	else
		return BME280_INIT_ERROR;
}
#endif


/*   SI1145Init  */
/**
 * Created on: Jul 30, 2021
 * Last Edited: Jul 30, 2021
 * Author: Akshay, Ajmi
 *
 * @brief initialize the sensor using SI1145_init API and check if sensor is initialized
 * @param none
 * @retval sensor init status
 *
 **/

#ifdef ENABLE_SI1145_UVINDEX
bool SI1145Init()
{
	/*enable UV module power*/
	enable(SI1145_POWER);
	HAL_Delay(50);
	bool UVInitstatus = Si1145_Init();
	HAL_Delay(50);
	if (!UVInitstatus)
		return SI1145_INIT_SUCCESS;
	else
		return SI1145_INIT_ERROR;

}
#endif

/*  readBME280  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021
 * Author: Akshay,Sruthi, Ajmi
 *
 * @brief read data from available API
 * @param sensor data pointer that contains the variables to be read from the sensor
 * @retval none
 *
 **/
#ifdef ENABLE_BME280
void readBME280(awsSensors_t *sensor_data) {

	if (!bmp280_read_float(&bme280Sensor1, &sensor_data->temperature,
			&sensor_data->pressure, &sensor_data->humidity)) {

		sensor_data->temperature = -1;
		sensor_data->pressure = -1;
		sensor_data->humidity = -1;
	}
	disable(BME280_POWER);
}
#endif

/*  readUVIndex  */
/**
 * Created on: Jul 30 2021
 * Last Edited: Jul 30 2021
 * Author: Akshay,Ajmi
 *
 * @brief read data from available API
 * @param sensor data pointer that contains the variables to be read from the sensor
 * @retval none
 *
 **/
#ifdef ENABLE_SI1145_UVINDEX

void readUVIndex(awsSensors_t *sensor_data)
{

	sensor_data->UVindex = Si1145_readUV();  /*  we getting an index value which multiplied by 100(from sensor) */

	disable(SI1145_POWER);

}
#endif


/*  weatherStationGPIO_Init  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief initialize all the gpio
 * @param none
 * @retval none
 *
 **/
void weatherStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HW_GPIO_Init(GPIOA, GPIO_PIN_8, &initStruct);
	HW_GPIO_Init(GPIOB, GPIO_PIN_15, &initStruct);
	HW_GPIO_Init(GPIOB, GPIO_PIN_2, &initStruct);
//	HW_GPIO_Init(GPIOB, GPIO_PIN_6, &initStruct);
}

/*  enable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief manual control of gpio
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {
	switch (pin) {
	case 1:
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); //for battery
		break;
	case 2:
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET); //for wind dir power
		break;
	case 3:
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET); //for sht20 isolated ground enable(mosfet gnd)
		break;
	}
}

/*  disable  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay
 *
 * @brief manual control of gpio
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {
	switch (pin) {
	case 1:
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET); //for battery
		break;
	case 2:
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET); //for wind dir power
		break;
	case 3:
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET); //for sht20 isolated ground enable(mosfet gnd)
		break;

	}
}


/*  weatherStationInit */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021
 * Author: Akshay, Ajmi
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void weatherStationInit() {

	MX_I2C1_Init();
#ifdef ENABLE_BME280
	bme280HWInit();
#endif
#ifdef ENABLE_SI1145_UVINDEX
	Si1145_Init();
#endif
#ifdef ENABLE_SHT20
	sht20init();
#endif
	rainGaugeInterruptEnable();
	windSpeedTimerEvent();
	weatherStationGPIO_Init();
}

/*  readWeatherStationParameters*/
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jul 30, 2021 by Ajmi
 * Author: Akshay, Ajmi
 *
 * @brief initialize  weather station system
 * @param none
 * @retval none
 *
 **/
void readWeatherStationParameters(awsSensors_t *sensor_data) {

	sensor_data->batteryLevel = readBatteryLevel();

#ifdef ENABLE_BME280
	bool bme280InitStatus = bme280IoInit();
	if (bme280InitStatus == BME280_INIT_SUCCESS) {
		readBME280(sensor_data);
	}
#endif


#ifdef ENABLE_SI1145_UVINDEX
	bool UVInitStatus = SI1145Init();
	if (UVInitStatus == SI1145_INIT_SUCCESS) {
		readUVIndex(sensor_data);
	}
#endif

#ifdef ENABLE_SHT20
	SHT20Read(sensor_data);
	sensor_data->windDirection = getWindDirection();
	sensor_data->rainfall = getAccumulatedRainfall();
	sensor_data->windSpeed = getWindSpeed();
	//disable(BME280_POWER);
#endif
}

